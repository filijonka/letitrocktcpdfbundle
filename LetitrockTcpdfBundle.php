<?php

namespace Letitrock\TcpdfBundle;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class LetitrockTcpdfBundle extends Bundle
{
    /**
     * @inheritdoc
     */
    public function boot()
    {
        if (!$this->container->hasParameter('letitrock_pdf.tcpdf')) {
            return;
        }

        // Define our TCPDF variables
        $config = $this->container->getParameter('letitrock_pdf.tcpdf');

        // TCPDF needs some constants defining if our configuration
        // determines we should do so (default true)
        // Set tcpdf.k_tcpdf_external_config to false to use the TCPDF
        // core defaults

        if ($config['k_tcpdf_external_config']){
            foreach ($config as $k => $v){
                $constKey = strtoupper($k);
                // All K_ constants are required and we add all other defined as well
                if ((preg_match("/^k_/i", $k)|| preg_match("/^pdf_/i", $k)) && !defined($constKey)){
                    $value = $this->container->getParameterBag()->resolveValue($v);
                    if (($k === 'k_path_cache' || $k === 'k_path_url_cache') && !is_dir($value)) {
                        $this->createDir($value);
                    }
                    define($constKey, $value);
                }
            }
        }

    }

    /**
     * Create a directory
     *
     * @param string $filePath
     *
     * @throws \RuntimeException
     */
    private function createDir($filePath)
    {
        $filesystem = new Filesystem();
        if (false === $filesystem->mkdir($filePath)) {
            throw new \RuntimeException(sprintf(
              'Could not create directory %s', $filePath
            ));
        }
    }

}
