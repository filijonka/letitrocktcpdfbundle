<?php

namespace Letitrock\TcpdfBundle\Pdf;

use Symfony\Component\HttpFoundation\Response;
use TCPDF;
use TCPDF_IMAGES;

class Pdf extends TCPDF
{
    /**
     * Holds the projectinfo that will be printed in the header
     * @var array
     */
    protected $projectInfo;

    /**
     * Pdf constructor.
     * @param string $orientation
     * @param string $unit
     * @param string $format
     * @param bool $unicode
     * @param string $encoding
     * @param bool $diskcache
     * @param bool $pdfa
     */
    public function __construct(
        $orientation = 'P',
        $unit = 'mm',
        $format = 'A4',
        $unicode = true,
        $encoding = 'UTF-8',
        $diskcache = false,
        $pdfa = false
    ) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->setHeaderMargin(PDF_MARGIN_HEADER);
        //$this->setFontSubsetting(true);
    }

    /**
     * Sets the documenttitle and header image
     * @param string $title
     */
    public function loadHeader($title, $width = PDF_HEADER_LOGO_WIDTH)
    {
        parent::setHeaderData(
            PDF_HEADER_LOGO,
            $width,
            $title
        );

        parent::setHeaderFont(['Helvetica', 'B', '15']);
    }

    /**
     * @inheritdoc
     */
    public function Header()
    {
        if ($this->header_xobjid === false) {
            // start a new XObject Template
            $this->header_xobjid = $this->startTemplate($this->w, $this->tMargin);
            $headerfont = $this->getHeaderFont();
            $headerdata = $this->getHeaderData();
            $this->y = $this->header_margin;
            $this->x = $this->original_lMargin;

            if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
                $imgtype = TCPDF_IMAGES::getImageFileType(K_PATH_IMAGES . $headerdata['logo']);
                if (($imgtype == 'eps') OR ($imgtype == 'ai')) {
                    $this->ImageEps(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
                } elseif ($imgtype == 'svg') {
                    $this->ImageSVG(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
                } else {
                    $tmpmargin = $this->rMargin;
                    $this->rMargin = $this->original_rMargin;
                    $this->Image(
                        K_PATH_IMAGES . $headerdata['logo'],
                        '',
                        '',
                        $headerdata['logo_width'],
                        0,
                        '',
                        '',
                        'T',
                        false,
                        300,
                        'R'
                    );
                    $this->rMargin = $tmpmargin;
                }
                $imgy = $this->getImageRBY();
            } else {
                $imgy = $this->y;
            }

            $cell_height = $this->getCellHeight($headerfont[2] / $this->k);
            // set starting margin for text data cell, do not include header logo since it is printed with R
            $header_x = $this->original_lMargin;

            $cw = $this->w - $this->original_lMargin - $this->original_rMargin - ($headerdata['logo_width'] * 1.1);

            $this->SetTextColorArray($this->header_text_color);
            // header title
            $this->SetFont($headerfont[0], 'B', $headerfont[2] + 1);
            $this->SetX($header_x);

            $this->Cell($cw, $cell_height, $headerdata['title'], 0, 1, 'C', 0, '', 0);
            // header string
            $this->SetFont($headerfont[0], $headerfont[1], $headerfont[2]);
            $this->SetX($header_x);
            $this->MultiCell($cw, $cell_height, $headerdata['string'], 0, '', 0, 1, '', '', true, 0, false, true, 0,
                'T', false);
            // print an ending header line
            $this->SetLineStyle(array(
                'width' => 0.85 / $this->k,
                'cap' => 'butt',
                'join' => 'miter',
                'dash' => 0,
                'color' => $headerdata['line_color']
            ));
            $this->SetY((2.835 / $this->k) + max($imgy, $this->y));
            if ($this->rtl) {
                $this->SetX($this->original_rMargin);
            } else {
                $this->SetX($this->original_lMargin);
            }
            $this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
            $this->endTemplate();
        }
        // print header template
        $x = 0;
        $dx = 0;

        $this->printProjectInfo();

        if (!$this->header_xobj_autoreset AND $this->booklet AND (($this->page % 2) == 0)) {
            // adjust margins for booklet mode
            $dx = ($this->original_lMargin - $this->original_rMargin);
        }
        if ($this->rtl) {
            $x = $this->w + $dx;
        } else {
            $x = 0 + $dx;
        }

        $this->printTemplate($this->header_xobjid, $x, 0, 0, 0, '', '', false);
        if ($this->header_xobj_autoreset) {
            // reset header xobject template at each page
            $this->header_xobjid = false;
        }
    }

    /**
     * @inheritdoc
     */
    public function footer()
    {
        $footer['gbg'] = array(
            'name' => 'ÅF Sprängkonsult',
            'phone' => 'Tel: 010-5050000',
            'street' => 'Box 1551',
            'postal' => '401 51 Göteborg',
        );

        $footer['tokholm'] = array(
            'name' => 'ÅF',
            'phone' => 'Tel: 010-5050000',
            'street' => ' ',
            'postal' => '169 99 Stockholm',
        );

        $footer['web'] = array(
            'gbg' => 'www.sprangkonsult.se',
            'tokholm' => 'www.afconsult.com',
        );

        $this->SetY(-25);

        // Classy italic font
        $this->SetFont('Times', 'I', 8);

        // Nice grey lines
        $this->SetDrawColor(200);

        // And make the line
        $this->Cell(0, 3, '', 'T');
        $this->Ln();

        $this->Cell(5, 3.5, '');
        $this->Cell(60, 3.5, $footer['gbg']['name']);
        $this->Cell(60, 3.5, $footer['tokholm']['name']);
        $this->Cell(60, 3.5, $footer['gbg']['phone']);
        $this->Ln();

        $this->Cell(5, 3.5, '');
        $this->Cell(60, 3.5, $footer['gbg']['street']);
        $this->Cell(60, 3.5, $footer['tokholm']['street']);
        $this->Ln();

        $this->Cell(5, 3.5, '');
        $this->Cell(60, 3.5, $footer['gbg']['postal']);
        $this->Cell(60, 3.5, $footer['tokholm']['postal']);
        $this->Ln();
        $this->Cell(5, 3.5, '');
        $this->Cell(60, 3.5, $footer['web']['gbg']);
        $this->Cell(60, 3.5, $footer['web']['tokholm']);
    }

    /**
     * Returns the projectinfo
     * @return array
     */
    public function getProjectInfo()
    {
        return $this->projectInfo;
    }

    /**
     * Sets the projectinfo
     * @param array $projectInfo
     */
    public function setProjectInfo($projectInfo)
    {
        $this->projectInfo = $projectInfo;
    }

    /**
     * @return mixed
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * Creates a response of the pdf
     * @param Response $response
     * @param string $title
     * @param int $projectNumber
     * @return Response
     */
    public function createResponse(Response $response, string $title, int $projectNumber)
    {
        $this->close();

        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set(
            'Cache-Control',
            'private, must-revalidate, post-check=0, pre-check=0, max-age=1'
        );
        $response->headers->set('Content-Length', $this->bufferlen);
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s').' GMT');
        $response->headers->set(
            'Content-Disposition',
            'inline; filename="'.basename($title . ' '. $projectNumber).'"'
        );

        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));

        $response->setContent($this->buffer);

        return $response;

    }
    private function printProjectInfo(int $extendedWidth = 4)
    {
        if (!empty($this->getProjectInfo())) {
            $this->setFont('helvetica', 'B', '9');
            foreach ($this->getProjectInfo() as $title => $text) {
                $this->Cell($this->GetStringWidth($title)+2, 11, $title . ':', 0);
                $this->Cell($this->GetStringWidth($title) + $extendedWidth, 11, $text, 0, 1);
            }
        }
    }

}
